<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inputPostalCode</name>
   <tag></tag>
   <elementGuidId>5597b37c-c2cb-43fc-8fb1-e17295d91720</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#postal-code</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='postal-code']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3488e626-dc79-4832-b861-f70b3c516153</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input_error form_input</value>
      <webElementGuid>397faff7-0c26-4e0b-86f4-a2876e3872f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Zip/Postal Code</value>
      <webElementGuid>c7ddab36-84b2-44e9-8c39-7ebc8d482712</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>ac880dab-a362-4e91-9137-f2f5e4b4bbed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>postalCode</value>
      <webElementGuid>9fed80e6-7388-4826-8dd7-f03715364917</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>postal-code</value>
      <webElementGuid>6d302080-4459-4bc0-9b6d-87fa9119d87a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>postalCode</value>
      <webElementGuid>0d40a075-5b9e-401f-834f-f2aa164d76b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>777edce0-185f-4c00-879b-f4f692f65db2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>f40309e7-87b6-4b4c-b15e-af40323e466b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;postal-code&quot;)</value>
      <webElementGuid>502c6f78-4d0f-4de3-89a7-efa537e61e02</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='postal-code']</value>
      <webElementGuid>0de6b0ce-ac21-4d9c-ac86-c6a5d0103480</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout_info_container']/div/form/div/div[3]/input</value>
      <webElementGuid>d6fd652a-4d72-4c9a-a272-aa19065c89c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>f280d6a0-9352-405b-bdd2-aefa8ebf1ff3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Zip/Postal Code' and @type = 'text' and @id = 'postal-code' and @name = 'postalCode']</value>
      <webElementGuid>7c0979cd-aae0-4efc-81da-fd288a4ac1e6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
